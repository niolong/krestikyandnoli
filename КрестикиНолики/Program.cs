﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace КрестикиНолики
{
    class Program
    {
        enum Cell
        {
            free,
            cellX,
            cellO
        }

        static void Main(string[] args)
        {
            #region Ввод переменных
            int size = 3;
            Cell[,] field = new Cell[size, size];

            int coordI = -1, coordJ = -1;
            int coordIuser2 = -1, coordJuser2 = -1;
            int step = 0;
            bool inputResult;

            bool game = true;
            #endregion

            #region Заполнение массива
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    field[i, j] = Cell.free;
                }
            }

            #endregion

            #region Вывод на экран
            Console.Clear();
            Console.WriteLine("Крестики нолики");

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    switch (field[i, j])
                    {
                        case Cell.free:
                            Console.Write("{0,-3}", "-");
                            break;
                        case Cell.cellX:
                            Console.Write("{0,-3}", "X");
                            break;
                        case Cell.cellO:
                            Console.Write("{0,-3}", "O");
                            break;
                    }
                }
                Console.WriteLine();
            }

            #endregion

            #region Игровой цикл

            do
            {
                #region Ход игрока 1
                do
                {
                    Console.WriteLine("Ход игрока 1");

                    Console.WriteLine("Введите координату i");
                    inputResult = int.TryParse(Console.ReadLine(), out coordI);

                    Console.WriteLine("Введите координату j");
                    inputResult = int.TryParse(Console.ReadLine(), out coordJ);
                } while (inputResult == false || coordI > size || coordJ > size
                || field[coordI - 1, coordJ - 1] == Cell.cellX
                || field[coordI - 1, coordJ - 1] == Cell.cellO
                || coordI <= 0 || coordJ <= 0);

                field[coordI - 1, coordJ - 1] = Cell.cellX;
                #endregion

                #region Проверка на победу игрока 1
                if (field[0, 0] == Cell.cellX && field[1, 1] == Cell.cellX && field[2, 2] == Cell.cellX
                   || field[0, 2] == Cell.cellX && field[1, 1] == Cell.cellX && field[2, 0] == Cell.cellX
                   || field[0, 0] == Cell.cellX && field[0, 1] == Cell.cellX && field[0, 2] == Cell.cellX
                   || field[1, 0] == Cell.cellX && field[1, 1] == Cell.cellX && field[1, 2] == Cell.cellX
                   || field[2, 0] == Cell.cellX && field[2, 1] == Cell.cellX && field[2, 2] == Cell.cellX
                   || field[0, 0] == Cell.cellX && field[1, 0] == Cell.cellX && field[2, 0] == Cell.cellX
                   || field[0, 1] == Cell.cellX && field[1, 1] == Cell.cellX && field[2, 1] == Cell.cellX
                   || field[0, 2] == Cell.cellX && field[1, 2] == Cell.cellX && field[2, 2] == Cell.cellX)
                {
                    Console.WriteLine("Победил 1й игрок");
                    game = false;
                    Console.ReadKey();
                }
                step++;

                if (step >= 9)
                {
                    Console.WriteLine("Ничья");
                    game = false;
                    Console.ReadKey();
                }
                #endregion

                #region Вывод на экран
                Console.Clear();
                Console.WriteLine("Крестики нолики");

                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        switch (field[i, j])
                        {
                            case Cell.free:
                                Console.Write("{0,-3}", "-");
                                break;
                            case Cell.cellX:
                                Console.Write("{0,-3}", "X");
                                break;
                            case Cell.cellO:
                                Console.Write("{0,-3}", "O");
                                break;
                        }
                    }
                    Console.WriteLine();
                }

                #endregion

                #region Ход игрока 2
                if (game)
                {
                    do
                    {
                        Console.WriteLine("Ход игрока 2");

                        Console.WriteLine("Введите координату i");
                        inputResult = int.TryParse(Console.ReadLine(), out coordIuser2);

                        Console.WriteLine("Введите координату j");
                        inputResult = int.TryParse(Console.ReadLine(), out coordJuser2);

                    } while (inputResult == false || coordIuser2 > size || coordJuser2 > size
                           || field[coordIuser2 - 1, coordJuser2 - 1] == Cell.cellO
                           || field[coordIuser2 - 1, coordJuser2 - 1] == Cell.cellX
                           || coordIuser2 <= 0 || coordJuser2 <= 0);

                    field[coordIuser2 - 1, coordJuser2 - 1] = Cell.cellO;
                }
                #endregion

                #region Проверка на победу игрока 2
                if (field[0, 0] == Cell.cellO && field[1, 1] == Cell.cellO && field[2, 2] == Cell.cellO
                   || field[0, 2] == Cell.cellO && field[1, 1] == Cell.cellO && field[2, 0] == Cell.cellO
                   || field[0, 0] == Cell.cellO && field[0, 1] == Cell.cellO && field[0, 2] == Cell.cellO
                   || field[1, 0] == Cell.cellO && field[1, 1] == Cell.cellO && field[1, 2] == Cell.cellO
                   || field[2, 0] == Cell.cellO && field[2, 1] == Cell.cellO && field[2, 2] == Cell.cellO
                   || field[0, 0] == Cell.cellO && field[1, 0] == Cell.cellO && field[2, 0] == Cell.cellO
                   || field[0, 1] == Cell.cellO && field[1, 1] == Cell.cellO && field[2, 1] == Cell.cellO
                   || field[0, 2] == Cell.cellO && field[1, 2] == Cell.cellO && field[2, 2] == Cell.cellO)
                {
                    Console.WriteLine("Победил 2й игрок");
                    game = false;
                    Console.ReadKey();
                }
                step++;
                #endregion

                #region Вывод на экран
                Console.Clear();
                Console.WriteLine("Крестики нолики");

                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        switch (field[i, j])
                        {
                            case Cell.free:
                                Console.Write("{0,-3}", "-");
                                break;
                            case Cell.cellX:
                                Console.Write("{0,-3}", "X");
                                break;
                            case Cell.cellO:
                                Console.Write("{0,-3}", "O");
                                break;
                        }
                    }
                    Console.WriteLine();
                }

                #endregion

            } while (game);
            #endregion

            Console.ReadKey();
        }
    }
}
